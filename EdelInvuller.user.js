// ==UserScript==
// @name        EdelInvuller
// @version     1.0
// @author      Unfriendly Sander
// @grant       none
// @include     https://nl*.tribalwars.nl*screen=place*
// @updateURL    https://bitbucket.org/twnl/edelinvuller/raw/a51078ce8bfa673d056de3bf4c029b103b5c571c/EdelInvuller.user.js
// @downloadURL  https://bitbucket.org/twnl/edelinvuller/raw/a51078ce8bfa673d056de3bf4c029b103b5c571c/EdelInvuller.user.js
// ==/UserScript==
 
var Edels = function() {
    var self = this;
    var archers;
    var template;
     
    self.init = function() {
        if (!localStorage.getItem('edels_boog') || !localStorage.getItem('edels_template')) {
            var archers = $('[data-unit="archer"]').length > 0;
            var template = {
                spear: 0,
                sword: 0,
                axe: 55,
                archer: 0,
                spy: 0,
                light: 30,
                marcher: 0,
                heavy: 0,
                ram: 0,
                catapult: 0
            };
             
             
            if (!archers) {
                delete template.archer;
                delete template.marcher;
            }
             
            localStorage.setItem('edels_boog', archers);
            localStorage.setItem('edels_template', JSON.stringify(template));
        }
         
        $('#linkContainer').append('- <a class="footer-link" href="#" id="edelinvuller">Edel Invuller</a>');
        self.handlers();
    };
     
    self.exec = function() {
        archers = localStorage.getItem('edels_boog') === 'true';
        template = JSON.parse(localStorage.getItem('edels_template'));
    };
     
    self.handlers = function() {        
        var getImg = function(unit) {
            return '<img src="http://dsnl.innogamescdn.com/8.31.1/24156/graphic/unit/unit_' + unit + '.png?db2c3">';
        };
         
        self.exec();
         
        $('#troop_confirm_train').on('click', function() {
            var $inputs = $('#place_confirm_units').find('input');
            var value;
             
            for (var prop in template) {
                if (template.hasOwnProperty(prop)) {
                    if (template[prop] == 0) {
                        value = '';
                    } else {
                        value = template[prop];
                    }
                     
                    $inputs.filter('[data-unit="' + prop + '"]').val(value);
                }
            }
        });
         
        $('#edelinvuller_save').on('click', function(e) {
            e.preventDefault();
             
            var $inputs = $('#edelinvuller_popup').find('input');
            var unit;
 
            for (var i = 0, l = $inputs.length; i < l; i++) {
                unit = $inputs.eq(i).attr('data-unit');
                template[unit] = $inputs.eq(i).val();
            }
 
            localStorage.setItem('edels_template', JSON.stringify(template));
            $('#edelinvuller_popup').remove();
            self.exec();
        });
         
        $('#edelinvuller').on('click', function(e) {
            e.preventDefault();
             
            if (!$('#edelinvuller_popup').length) {
                var thStyle = 'style="text-align:center;padding: 0 10px;"';
                var popupWidthPx = 550;
                var popupStyle = 'position:fixed;overflow:auto;top:100px;max-height:' + (window.outerHeight - 250) + 'px;width:' + popupWidthPx + 'px;left:' + (window.outerWidth / 2 - (popupWidthPx / 2)) + 'px;background-color:#e8d4a9;border:2px solid #804000;padding:10px;text-align:center;font-size:12px;z-index:100;box-shadow:0 0 50px 0px #000000;';
                var popup = '<div id="edelinvuller_popup" style="' + popupStyle + '"><h3>Edel Invuller</h3><table class="vis" style="margin-left: auto; margin-right: auto;"><thead><tr>';
                var datacells = '';
                 
                for (var prop in template) {
                    if (template.hasOwnProperty(prop)) {
                        popup += '<th width="65" ' + thStyle + ' >' + getImg(prop) + '</th>';
                        datacells += '<td><input type="text" style="text-align: center;width: 80%" value="' + template[prop] + '" data-unit="' + prop + '"></td>';
                    }
                }
                 
                popup += '</tr></thead><tbody><tr>' + datacells + '</tr></tbody></table><a id="edelinvuller_save" href="#" class="btn" style="display:block;margin-top:10px;width:50%;margin-left: 25%;">Save</a></div>';
                 
                 
                $('body').append(popup);
                self.handlers();
            }
        });
    };
}
 
$(document).ready( doInit );
 
function doInit() {
    if (document.location.toString().match(/try=confirm/)) {
        var edels = new Edels();
        edels.init();
    }
}